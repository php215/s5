<?php

session_start();

class TaskList{
    // method for adding a new task to the task list
    public function add($description){
        // create a new Task object based on the task added by the user. isFinished defaults to false
        $newTask = (object)[
            'description' => $description,
            'isFinished' => false
        ];

        // $_SESSION is another kind of global variable typically used by the server to send data in its responses
        // if the $_SESSION['tasks'] array does not yet exist, create it.
        if($_SESSION['tasks'] === null){
            $_SESSION['tasks'] = array(); 
        }

        // push the $newTask object to the $_SESSION['tasks'] array
        array_push($_SESSION['tasks'], $newTask);
    }

    public function update($id, $description, $isFinished){
        $_SESSION['tasks'][$id]->description = $description;
        $_SESSION['tasks'][$id]->isFinished = ($isFinished !== null) ? true : false;
    }

    public function remove($id){
        array_splice($_SESSION['tasks'], $id, 1);
    }
}

$taskList = new TaskList(); // create a new taskList object

if($_POST['action'] === 'add'){
    $taskList->add($_POST['description']); // call the add method from tasklist and pass the task description to it
} else if($_POST['action'] === 'update'){
    $taskList->update($_POST['id'], $_POST['description'], $_POST['isFinished']);
} else if(($_POST['action'] === 'remove')){
    $taskList->remove($_POST['id']);
} else if(($_POST['action'] === 'clear')){
    session_destroy();
}

// redirect the user back to index
header('Location: ./index.php');