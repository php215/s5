<?php require_once "./code.php" ?>
<?php $tasks = ['Get git', 'Bake HTML', 'Eat CSS', 'Learn PHP'];
    // $_GET is an example of a super global variable in PHP. Super global variables are just like regular variables in that they hold data. The difference is that super global variables can send data to and from servers.

    // $_POST is another example of a super global variable in PHP. It has the same purpose as $_GET but does not generate a URL parameter, making it more fitting for private / personal data
    
    // isset chacks if the named super global variable exists or not yet. If it does, it returns true. If it does, it returns false.
    if (isset($_GET['index'])){
        $indexGet = $_GET['index'];
        echo "The retrieved task from GET is $tasks[$indexGet]";
    }

    if (isset($_POST['index'])){
        $indexPost = $_POST['index'];
        echo "The retrieved task from POST is $tasks[$indexPost]";
    }
?>

<!DOCTYPE html>

<html>

    <head>

        <title>S05: Client-Server Communication (GET and POST)</title>

    </head>

    <body>

        <h1>Task index from GET</h1>

        <form method="GET">

            <select name="index" required>
                <option value="0">0</option>
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
            </select>

            <button type="submit">GET</button>

        </form>

        <h1>Task index from POST</h1>

        <form method="POST">

            <select name="index" required>
                    <option value="0">0</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
            </select>

            <button type="submit">POST</button>

        </form>

    </body>

</html>